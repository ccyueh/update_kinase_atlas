#!/usr/bin/python
import sys
from sys import argv
import urllib2

def query_sub(year,month,day):
    url = 'http://www.rcsb.org/pdb/rest/search'
    y = '20' + year
    m = ('0'+month)[-2:]
    d = ('0'+day)[-2:]
    date = '-'.join([y,m,d])
   
    queryText = """
    <orgPdbCompositeQuery version="1.0">
        
        <queryRefinement>
            <queryRefinementLevel>0</queryRefinementLevel>
            <orgPdbQuery>
                <queryType>org.pdb.query.simple.TreeEntityQuery</queryType>
                <description>MolecularFunctionTree Search for protein kinase activity  (GO ID:4672)</description>
                <t>6</t>
                <n>4672</n>
                <nodeDesc>protein kinase activity  (GO ID:4672)</nodeDesc>
            </orgPdbQuery>
        </queryRefinement>
 
        <queryRefinement>
            <queryRefinementLevel>1</queryRefinementLevel>
            <conjunctionType>and</conjunctionType>
            <orgPdbQuery>
                <queryType>org.pdb.query.simple.PfamIdQuery</queryType>
                <description>Pfam Accession Number PF00069 </description>
                <pfamID>PF00069</pfamID>
            </orgPdbQuery>
        </queryRefinement>

        <queryRefinement>
            <queryRefinementLevel>2</queryRefinementLevel>
            <conjunctionType>and</conjunctionType>
            <orgPdbQuery>
                <queryType>org.pdb.query.simple.ReleaseDateQuery</queryType>
                <description>Released on """ + date + """ and later</description>
                <pdbx_audit_revision_history.revision_date.comparator>between</pdbx_audit_revision_history.revision_date.comparator>
                <pdbx_audit_revision_history.revision_date.min>""" + date + """</pdbx_audit_revision_history.revision_date.min>
                <pdbx_audit_revision_history.ordinal.comparator>=</pdbx_audit_revision_history.ordinal.comparator>
                <pdbx_audit_revision_history.ordinal.value>1</pdbx_audit_revision_history.ordinal.value>
            </orgPdbQuery>
        </queryRefinement>

    </orgPdbCompositeQuery>
    """

    req = urllib2.Request(url, data=queryText)
    f = urllib2.urlopen(req)
    f2 = f.read()
    if len(f2) > 0:
        with open(('list-' + year + m + d),'w') as g:
            g.write(f2)
    else:
        print "No new kinases found."

#date format YYMMDD
argv.pop(0)
date = argv[0]
year = date[0:2]
month = date[2:4]
day = date[-2:]
query_sub(year,month,day)
