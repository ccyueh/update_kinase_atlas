# Updating the Kinase Atlas

Updating the Kinase Atlas requires identifying and downloading new kinase structures that have been released since the previous update. Run *get_kinase_files.sh ${last_update_folder_name}* to find, download, and split new kinase structures into N- and C-terminal domains. The domain structures should then be mapped using FTMap/Atlas. After mapping, the consensus sites should be assigned to different potential allosteric sites using *match_kinase_sites.py*. All of these steps should be performed in a new folder named as the current date (YYMMDD), located in the same directory as the most recent update (which is referenced to avoid re-downloading kinases already in the Atlas). "190503" is included here as a reference/most recent update, and site_pdb/ and 3hrf*.pdb are files needed for the consensus site matching/kinase domain splitting scripts.

### Summary of Steps:
1. In the same directory containing the last update (such as 190503), create a folder with today's date (YYMMDD) and cd into that directory.
2. Run *get_kinase_files.sh ${last_update_folder_name}* to get new kinase structures.
3. Map the split kinase domains in the dom/ folder.
4. Bring the mapped structures back into the dom/ folder and run *match_kinase_sites.sh* to assign consensus sites to allosteric sites.