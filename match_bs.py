#!/usr/bin/python

import __main__
__main__.pymol_argv = ['pymol','-qc']

from sys import argv
import glob
import numpy as np
from scipy.spatial import Delaunay,distance

def match_bs(coord_file):
    match = {}
    coord = np.loadtxt(coord_file)
    
    for site in site_coords:
        match[site] = calc_overlap(expand_coord(site_coords[site],1),coord)

    if max(match.values()) > 0:
        max_loc = list(np.where(np.asarray(match.values()) == max(match.values()))[0])
        if len(max_loc) > 1:
            dist = {}
            for m in max_loc:
                site_match = match.keys()[m]
                d = distance.cdist(site_coords[site_match],coord).flatten()
                dist[min(d)] = site_match
            best_match = dist[min(dist.keys())]
        else:
            best_match = match.keys()[max_loc[0]]
        line = '\t'.join(coord_file.split('/')[1].split('_') + [best_match]) + '\n'
        f.write(line)

def load_sites():
    site_coords = {}
    for site_file in glob.glob('../site_pdb/coord/*'):
        site = site_file.split('/')[-1]
        coord = np.loadtxt(site_file)
        site_coords[site] = coord 

    return site_coords

def expand_coord(coord,extra):
    new_coord = []
    for xyz in coord:
        x,y,z = xyz
        new_coord.append([x+extra,y+extra,z+extra])
        new_coord.append([x-extra,y-extra,z-extra])

    return np.asarray(new_coord)

def calc_overlap(site,cs):
    hull = Delaunay(site)
    h = hull.find_simplex(cs)>-1
    match = np.sum(h)

    return match

site_coords = load_sites()
coord_list = glob.glob('coord/*')
f = open('table-pdb-cs-site','w')
for coord in coord_list:
    match_bs(coord)
