#!/usr/bin/python
from __future__ import print_function
import __main__
__main__.pymol_argv = ['pymol','-qc']

import sys
from sys import argv
import numpy as np

import pymol,glob
from pymol import cmd, util, stored
pymol.finish_launching()

cmd.feedback('disable','all','warnings')

def splitdom(loc3hrf,pdbloc):
    load3hrf(loc3hrf)

    bestfit = []
    cmd.load((pdbloc))
    pdbname = pdbloc.split('/')[-1].split('.')[0]
        
    fitalign = cmd.align(pdbname,'3hrf')[0]
    fitsuper = cmd.super((pdbname + ' & alt A+\'\''),'3hrf & alt B+\'\'')[0]
    if fitalign < fitsuper or fitsuper == 0:
        fit = cmd.align(pdbname,'3hrf')[0]
    else:
        fit = fitsuper
    
    colorterm = {'yelloworange':'n','purpleblue':'c'}
    listresi = getresi(pdbname)
    if fit < 4:
        whichdom(pdbname,listresi)
        for c in colorterm.keys():
            if len(getcolor(pdbname,c)) > 0:
                fillgaps(pdbname,c)
                checkfrag(pdbname,listresi,c)
        expand(pdbname,listresi)
        for c in colorterm.keys():
            if len(getcolor(pdbname,c)) > cmd.count_atoms('3hrf and n. ca')/5:
                cmd.save((pdbname + '_' + colorterm[c] + '.pdb'),('color ' + c))
    cmd.reinitialize()

def expand(pdbname,listresi):
    white = np.array(getcolor(pdbname,'bluewhite'))
    yellow = getcolor(pdbname,'yelloworange')
    purple = getcolor(pdbname,'purpleblue')
   
    ntail,inter,ctail = [[],[],[]]
    if len(yellow) > 0:
        ntail = list(white[np.where(white < min(yellow))])
        ntail.reverse()
    if len(purple) > 0:
        ctail = list(white[np.where(white > max(purple))])
    if len(yellow) > 0 and len(purple) > 0:
        inter = list(white[np.where((white > max(yellow)) & (white < min(purple)))])
    
    doms = {'n':'3hrf_A_1','c':'3hrf_A_2'}
    tails = {'n':ntail,'c':ctail}
    colors = {'n':'yelloworange','c':'purpleblue'}
    for term in ['n','c']:
        for r in tails[term]:
            select_r = pdbname + ' and resi ' + str(r)
            near_r = cmd.count_atoms((select_r + ' w. 5 of ' + doms[term]))
            if near_r > 0:
                cmd.color(colors[term],select_r)
            else:
                break

    color_i = colors['n']
    for i in inter:
        select_i = pdbname + ' and resi ' + str(i)
        near_i = {}
        for term in ['n','c']:
            near_i[term] = cmd.count_atoms((select_i + ' w. 3 of ' + doms[term]))
        
        if near_i['n'] < near_i['c']:
            color_i = colors['c']
        cmd.color(colors['n'],select_i)

def checkfrag(pdbname,listresi,color):
    listcolor = getcolor(pdbname,color)
    resi = str(listcolor[0]) + '-' + str(listcolor[-1])
    if iscolor(pdbname,resi,color) != 0:
        gaps = getgap([listresi[0]] + listcolor + [listresi[-1]])
        for g in range(len(gaps)-1):
            gap = str(gaps[g]) + '-' + str(gaps[g+1])
            if iscolor(pdbname,gap,color) == 0:
                if (gaps[g+1] - gaps[g]) < 50:
                    cmd.color('bluewhite',(pdbname + ' and resi ' + gap))

def fillgaps(pdbname,color):
    listcolor = getcolor(pdbname,color)
    gaps = getgap(listcolor)
    if len(gaps) > 0:
        for resi_ind in range(len(gaps)-1):
            resi_range = range(gaps[resi_ind]+1,gaps[resi_ind+1])
            if 50 > len(resi_range) > 0:
                resi = str(resi_range[0]) + '-' + str(resi_range[-1])
            else:
                resi = 'notgap'
            if iscolor(pdbname,resi,'bluewhite') == 0:
                cmd.color(color,(pdbname + ' and resi ' + resi))

def whichdom(pdbname,listresi):
    dom = {'n':'3hrf_A_1','c':'3hrf_A_2'}
    side = {'n':len(listresi)-1,'c':0}
    end = {}
    for t in ['n','c']:
        if cmd.count_atoms((dom[t] + ' and n. ca w. 3 of ' + pdbname + ' and n. ca')) > cmd.count_atoms((dom[t] + ' and n. ca'))/2:
            if len(getends(pdbname,t)) == 0:
                end[t] = listresi[side[t]]
            else:
                end[t] = int(getends(pdbname,t)[0])
    if len(end) > 0:
        for i in listresi:
            n = cmd.count_atoms(('3hrf_A_1 and n. ca w. 3 of ' + pdbname + ' and resi ' + str(i) + ' and n. ca'))       
            c = cmd.count_atoms(('3hrf_A_2 and n. ca w. 3 of ' + pdbname + ' and resi ' + str(i) + ' and n. ca'))
            if i < end[end.keys()[-1]] and n > 0:
                cmd.color('yelloworange',(pdbname + ' and resi ' + str(i)))
            elif i > end[end.keys()[0]] and c > 0:
                cmd.color('purpleblue',(pdbname + ' and resi ' + str(i)))
            else:
                cmd.color('bluewhite',(pdbname + ' and resi ' + str(i)))

def iscolor(pdbname,resi,color):
    select_range = pdbname + ' and resi ' + resi
    select_color = select_range + ' and color ' + color
    color_diff = cmd.count_atoms(select_range) - cmd.count_atoms(select_color)
    
    return color_diff

def getcolor(pdbname,color):
    stored.listall = []
    cmd.iterate((pdbname + ' and color ' + color + ' and n. ca'),'stored.listall.append((resi))')
    
    listint = []
    for i in stored.listall:
        if i.isdigit():
            listint.append(int(i))

    return sorted(listint)

def getends(pdbname,term):
    termresi = {'n':'165','c':'166'}
    stored.t = []
    cmd.iterate((pdbname + ' and n. ca w. 3 of 3hrf and resi ' + termresi[term] + ' and n. ca'),'stored.t.append((resi))')

    return stored.t

def getresi(pdbname):
    stored.listall = []
    cmd.iterate((pdbname + ' and pol. and n. ca'),'stored.listall.append((resi))')
    
    listint = []
    for i in stored.listall:
        if i.isdigit():
            listint.append(int(i))

    return sorted(listint)

def getgap(listresi):
    listgap = []
    for i in range(len(listresi)-1):
        if listresi[i+1] > listresi[i]+1:
            listgap.append(listresi[i])
            listgap.append(listresi[i+1])

    return sorted(list(set(listgap)))

def load3hrf(loc):
    cmd.load((loc + '3hrf.pdb'))
    cmd.load((loc + '3hrf_A_1.pdb'))
    cmd.load((loc + '3hrf_A_2.pdb'))

argv.pop(0)
loc3hrf = '../../'
for pdbloc in argv:
    splitdom(loc3hrf,pdbloc)
