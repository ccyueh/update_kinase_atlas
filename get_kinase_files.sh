last_update=$1

../get_new_kinase_list.py $last_update

cat list-$last_update | cut -c 1-4 | sort -u | while read line
do 
	echo $line $(grep -c $line ../${last_update}/table-pdb-cs-site)
done | awk '{if ($2==0) print $1}' > list-new

mkdir pdb
mkdir pdbc
mkdir dom
mkdir coord

cd pdb
cat ../list-new | while read line
do 
	wget https://files.rcsb.org/download/${line}.pdb
done

ls *.pdb | grep -v '_' | while read line
do 
	pdb_split_chains $line
done
mv *_*.pdb ../pdbc/

cd ../dom
ls ../pdbc/*.pdb | xargs -n 1 -P 8 ../../split_dom.py

# map kinase domain structures
