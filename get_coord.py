#!/usr/bin/python

import __main__
__main__.pymol_argv = ['pymol','-qc']

from sys import argv
import numpy as np
import pymol,glob
from pymol import cmd, util, stored
pymol.finish_launching()

cmd.feedback('disable','all','warnings')

def get_coord(pdb):
    cmd.load(pdb)
    cs_list = cmd.get_object_list('(consensus.*)')
    for c in cs_list:
        rank = c.split('.')[1].lstrip('0')
        if len(rank) == 0:
            rank = '0'
        size = c.split('.')[2].lstrip('0')

        coord = cmd.get_coords(c,1)
        coord_name = '../coord/' + '_'.join(pdb.split('/')[-1].split('_')[0:3] + [rank,size])
        np.savetxt(coord_name,coord,fmt='%.3f')
    if len(cs_list) == 0:
        coord = cmd.get_coords('(all)',1)
        coord_name = '../coord/' + pdb.split('/')[-1].split('.')[0]
        np.savetxt(coord_name,coord,fmt='%.3f')

argv.pop(0)
get_coord(argv[0])
